module PeerCommander
  module Errors
    CommandNotExecutedError = Class.new(StandardError)
  end
end
