module PeerCommander
  module Errors
    CommandAlreadyExecutedError = Class.new(StandardError)
  end
end
