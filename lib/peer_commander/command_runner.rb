require_relative "command"
require_relative "parallel_executor"

module PeerCommander
  # Runs the listed commands with a configurable level of parallelism
  class CommandRunner
    def initialize(commands)
      @commands = commands
      @command_results = []
    end

    def execute(parallelism: 1)
      start = Time.now
      @command_results = ParallelExecutor.new.execute(commands, parallelism)
      @duration = Time.now - start
      @command_results
    end

    def success?
      raise Errors::CommandNotExecutedError if command_results.empty?

      @command_results.all?(&:success?)
    end

    def failed?
      !success?
    end

    def all_commands
      raise Errors::CommandNotExecutedError if command_results.empty?

      @command_results
    end

    def successful_commands
      raise Errors::CommandNotExecutedError if command_results.empty?

      @command_results.select(&:success?)
    end

    def failed_commands
      raise Errors::CommandNotExecutedError if command_results.empty?

      @command_results.select(&:failed?)
    end

    def duration
      raise Errors::CommandNotExecutedError if command_results.empty?

      @duration
    end

    private

    attr_reader :commands, :command_results
  end
end
