# Peer::Commander

Provides an interfacing for executing mutliple system commands with a configurable level of parallelism.

The result objects will have access to the output of the command as well as the return status.

The overall result of executing the commands will be successful if all of the commands executed completed with
a successful return code.

Internally commands are executed with Open3.capture2e, env, opts, and stdin\_data used to init the commands are
sent directly to Open3.capture2e, see the documentation for that class to understand the behaviour and options
available.

## Installation

Add this line to your application's Gemfile:

```ruby
gem "peer_commander"
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install peer_commander

## Usage

This is a convoluted example to demonstrate all the features

```ruby
require "peer_commander"

commands = [
  PeerCommander::Command.new("sleep 2"),
  PeerCommander::Command.new("wibble"),
  PeerCommander::Command.new("sleep 1"),
  PeerCommander::Command.new("exit 1"),
  PeerCommander::Command.new("echo $TEST_ENV", env: { "TEST_ENV" => "output from env var"}),
  PeerCommander::Command.new("cat", stdin_data: "data passed to standard in"),
  PeerCommander::Command.new("sleep 4", opts: { chdir: "/tmp" }), # This command will be executed in the directory /tmp
]

commander = PeerCommander::CommandRunner.new(commands)

results = commander.execute(parallelism: 4)

puts "SUCCESS" if commander.success?
puts "FAILED" if commander.failed?

commander.all_commands.each { |command| puts "Executed #{command.command}" }

commander.successful_commands.each do |command|
  puts "Command [#{command.command}] succeeded in #{command.duration} seconds with output:"
  puts command.output
  puts "================================================="
end

commander.failed_commands.each do |command|
  print "Command [#{command.command}] failed in #{command.duration} seconds "

  if command.exception
    puts "with exception #{command.exception}"
  else
    puts "with exit code #{command.exit_code} and output\n"
    puts command.output
  end

  puts "================================================="
end

puts "Total operation took #{commander.duration} seconds"
```

## Version numbering

This project will follow strict semantic versioning (https://semver.org/spec/v2.0.0.html) once we reach version 0.1.
Until then any version change can break anything.

## Development

You can run `bin/rspec` or `bin/rake` to run all the tests, you can also run `bin/console` for an interactive prompt
that will allow you to experiment.

## Contributing

Bug reports and pull requests are welcome on Bitbucket at https://bitbucket.org/eviljonny/peer_commander/

## License

This project is released under the MIT license which can be found in the LICENSE file
