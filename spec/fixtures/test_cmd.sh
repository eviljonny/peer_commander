#!/bin/bash

EXIT_CODE=$1
OPERATING_MODE=$2
SLEEP_TIME=$3

if [ -z "$SLEEP_TIME" ]; then
  SLEEP_TIME=0
fi

if [ -z "$EXIT_CODE" ]; then
  EXIT_CODE=0
fi

if [ -z "$OPERATING_MODE" ]; then
  OPERATING_MODE=stdout
fi

case $OPERATING_MODE in
  stderr)
    echo "STDERR_OUTPUT" >&2
    ;;
  both)
    echo "STDOUT_OUTPUT_1"
    echo "STDERR_OUTPUT_1" >&2
    echo "STDOUT_OUTPUT_2"
    echo "STDERR_OUTPUT_2" >&2
    ;;
  stdout)
    echo "STDOUT_OUTPUT"
    ;;
  echo_stdin)
    input=$(</dev/stdin)
    echo "$input"
esac

sleep $SLEEP_TIME

exit $EXIT_CODE
