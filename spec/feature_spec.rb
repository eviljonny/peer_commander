# Some of the feature tests are designed to test that the parallelism is working
# as intended by checking execution finishes within a sensible boundary of the expected
# time based on the parallelism and command duration
#
# Because of the length of time taken if we run the tests over and over I am choosing
# to group multiple expectations into single examples in order to cut the overall testng
# time down, I've kept the parallelism ones separate since they are the core of the functionality
# for this entire gem
RSpec.describe "Running commands", type: :feature do
  let(:runner) { PeerCommander::CommandRunner.new(commands) }
  let(:commands) do
    [
      PeerCommander::Command.new("spec/fixtures/test_cmd.sh 0 stdout 1"),
      PeerCommander::Command.new("spec/fixtures/test_cmd.sh 0 stderr 1")
    ]
  end

  context "Parallelism" do
    context "with 1 process - fewer processes than commands" do
      it "takes around 2 seconds" do
        runner.execute(parallelism: 1)
        expect(runner.duration).to be_within(0.5).of(2)
      end
    end

    context "with 2 processes - equal commands and processes" do
      it "takes around 1 second" do
        runner.execute(parallelism: 2)
        expect(runner.duration).to be_within(0.5).of(1)
      end
    end

    context "with 3 processes - more processes than commands" do
      it "takes around 1 second" do
        runner.execute(parallelism: 3)
        expect(runner.duration).to be_within(0.5).of(1)
      end
    end
  end

  context "Testing the external api" do
    context "with only successful commands" do
      it "behaves as advertised" do
        results = runner.execute(parallelism: 2)

        expect(results.any?(&:failed?)).to be false
        expect(results.size).to eq 2

        expect(runner.success?).to be true
        expect(runner.failed?).to be false
        expect(runner.all_commands).to eq results
        expect(runner.successful_commands).to eq results
        expect(runner.failed_commands).to eq []
        expect(runner.duration).to be_within(0.5).of(1)

        stdout_result = results.select { |command| command.command.match(/stdout/) }.first
        stderr_result = results.select { |command| command.command.match(/stderr/) }.first

        expect(stdout_result.command).to eq commands[0].command
        expect(stdout_result.output).to eq "STDOUT_OUTPUT\n"
        expect(stdout_result.exception).to be nil
        expect(stdout_result.success?).to be true
        expect(stdout_result.failed?).to be false
        expect(stdout_result.exit_code).to eq 0
        expect(stdout_result.executed?).to be_truthy
        expect(stdout_result.duration).to be_within(0.2).of(1)

        expect(stderr_result.command).to eq commands[1].command
        expect(stderr_result.output).to eq "STDERR_OUTPUT\n"
        expect(stderr_result.exception).to be nil
        expect(stderr_result.success?).to be true
        expect(stderr_result.failed?).to be false
        expect(stderr_result.exit_code).to eq 0
        expect(stderr_result.duration).to be_within(0.2).of(1)
        expect(stderr_result.executed?).to be_truthy
      end
    end

    context "with a successful command, a failed command, and a command that raised an exception" do
      let(:commands) do
        [
          PeerCommander::Command.new("spec/fixtures/test_cmd.sh 0 stdout 0.5"),
          PeerCommander::Command.new("spec/fixtures/test_cmd.sh 1 stderr 1"),
          PeerCommander::Command.new("spec/fixtures/not_existing_command")
        ]
      end

      it "behaves as advertised" do
        results = runner.execute(parallelism: 3)

        expect(results.select(&:failed?).size).to eq 2
        expect(results.select(&:success?).size).to eq 1
        expect(results.size).to eq 3

        expect(runner.success?).to be false
        expect(runner.failed?).to be true
        expect(runner.all_commands).to eq results
        expect(runner.successful_commands).to eq [results[0]]
        expect(runner.failed_commands).to eq results[1..2]
        expect(runner.duration).to be_within(0.2).of(1)

        stdout_result = results.select { |command| command.command.match(/stdout/) }.first
        stderr_result = results.select { |command| command.command.match(/stderr/) }.first
        exception_result = results.select { |command| command.command.match(/not_existing/) }.first

        expect(stdout_result.command).to eq commands[0].command
        expect(stdout_result.output).to eq "STDOUT_OUTPUT\n"
        expect(stdout_result.exception).to be nil
        expect(stdout_result.success?).to be true
        expect(stdout_result.failed?).to be false
        expect(stdout_result.exit_code).to eq 0
        expect(stdout_result.executed?).to be_truthy
        expect(stdout_result.duration).to be_within(0.2).of(0.5)

        expect(stderr_result.command).to eq commands[1].command
        expect(stderr_result.output).to eq "STDERR_OUTPUT\n"
        expect(stderr_result.exception).to be nil
        expect(stderr_result.success?).to be false
        expect(stderr_result.failed?).to be true
        expect(stderr_result.exit_code).to eq 1
        expect(stderr_result.duration).to be_within(0.2).of(1)
        expect(stderr_result.executed?).to be_truthy

        expect(exception_result.command).to eq commands[2].command
        expect(exception_result.output).to be nil
        expect(exception_result.exception).to be_kind_of StandardError
        expect(exception_result.success?).to be false
        expect(exception_result.failed?).to be true
        expect(exception_result.exit_code).to be nil
        expect(exception_result.duration).to be_within(0.2).of(0)
        expect(exception_result.executed?).to be_truthy
      end
    end

    context "when passing env, options, and stdin_data" do
      let(:commands) do
        [
          PeerCommander::Command.new("echo $TEST_ENV", env: { "TEST_ENV" => "ENV_VAL" }),
          PeerCommander::Command.new("pwd", opts: { chdir: "spec/" }),
          PeerCommander::Command.new("cat", stdin_data: "TEST_STDIN")
        ]
      end

      it "behaves as advertised" do
        results = runner.execute(parallelism: 3)

        expect(runner.success?).to be true

        echo_result = results.select { |command| command.command.match(/echo/) }.first
        pwd_result = results.select { |command| command.command.match(/pwd/) }.first
        stdin_result = results.select { |command| command.command.match(/cat/) }.first

        expect(echo_result.output).to eq "ENV_VAL\n"
        expect(pwd_result.output).to eq File.absolute_path(File.join(Dir.pwd, "spec")) + "\n"
        expect(stdin_result.output).to eq "TEST_STDIN"
      end
    end
  end
end
