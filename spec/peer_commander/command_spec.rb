RSpec.describe PeerCommander::Command do
  subject { described_class.new(command) }
  let(:command) { "spec/fixtures/test_cmd.sh #{exit_code} #{output_mode} #{sleep_time}" }
  let(:failing_command) { "spec/fixtures/no_such_script" }
  let(:exit_code) { 0 }
  let(:output_mode) { "stdout" }
  let(:sleep_time) { 0 }

  describe "#execute" do
    it "executes the given command" do
      expect(Open3).to receive(:capture2e).with({}, command, {}).and_call_original

      subject.execute

      expect(subject.success?).to be true
    end

    context "when an env has been provided" do
      subject { described_class.new(command, env: env) }
      let(:env) { double }

      it "sends the env to Open3" do
        expect(Open3).to receive(:capture2e).with(env, command, {}).and_call_original
        subject.execute
      end
    end

    context "when extra options have been provided" do
      subject { described_class.new(command, opts: options) }
      let(:options) { { chdir: "/tmp" } }

      it "sends the options to Open3" do
        expect(Open3).to receive(:capture2e).with({}, command, chdir: "/tmp").and_call_original
        subject.execute
      end
    end

    it "raises CommandAlreadyExecutedError if it was already executed" do
      subject.execute
      expect { subject.execute }.to raise_error PeerCommander::Errors::CommandAlreadyExecutedError
    end

    it "returns self" do
      expect(subject.execute).to be subject
    end

    context "with stdin_data" do
      subject { described_class.new(command, stdin_data: stdin_data) }

      let(:output_mode) { "echo_stdin" }
      let(:stdin_data) { "SOME DATA\nLAST LINE" }

      it "sends the stdin_data as stdin to the command" do
        subject.execute

        expect(subject.output).to eq("SOME DATA\nLAST LINE\n")
      end
    end

    context "when the command raises an exception" do
      let(:command) { failing_command }
      FakeError = Class.new(StandardError)

      before do
        allow(Open3).to receive(:capture2e).with({}, command, {}).and_raise(FakeError)
      end

      it "does not propogate the exception" do
        expect { subject.execute }.not_to raise_error
      end

      it "captures the exception and puts it into the exception attribute" do
        subject.execute
        expect(subject.exception).to be_instance_of FakeError
      end
    end
  end

  describe "#success?" do
    context "with a successful command" do
      it "returns true" do
        subject.execute
        expect(subject.success?).to be true
      end
    end

    context "with a failing command" do
      let(:exit_code) { 1 }

      it "returns false" do
        subject.execute
        expect(subject.success?).to be false
      end
    end

    context "if an exception has been set" do
      let(:command) { failing_command }

      it "returns false" do
        subject.execute
        expect(subject.success?).to be false
      end
    end

    it "raises CommandNotExecutedError if the command hasn't been executed yet" do
      expect { subject.success? }.to raise_error PeerCommander::Errors::CommandNotExecutedError
    end
  end

  describe "#failed?" do
    context "with a successful command" do
      it "returns false" do
        subject.execute
        expect(subject.failed?).to be false
      end
    end

    context "with a failing command" do
      let(:exit_code) { 1 }

      it "returns true" do
        subject.execute
        expect(subject.failed?).to be true
      end
    end

    context "if an exception has been set" do
      let(:command) { failing_command }

      it "returns true" do
        subject.execute
        expect(subject.failed?).to be true
      end
    end

    it "raises CommandNotExecutedError if the command hasn't been executed yet" do
      expect { subject.success? }.to raise_error PeerCommander::Errors::CommandNotExecutedError
    end
  end

  describe "#output" do
    context "with output to stdout" do
      let(:output_mode) { "stdout" }

      it "returns output which went to stdout" do
        subject.execute
        expect(subject.output).to eq "STDOUT_OUTPUT\n"
      end
    end

    context "without output to stderr" do
      let(:output_mode) { "stderr" }

      it "returns output which went to stderr" do
        subject.execute
        expect(subject.output).to eq "STDERR_OUTPUT\n"
      end
    end

    context "without output to stdout and stderr" do
      let("output_mode") { "both" }

      it "retuns the stderr and stdout output interleaved as they were output" do
        subject.execute
        expect(subject.output).to eq(
          <<~END_OF_OUTPUT
            STDOUT_OUTPUT_1
            STDERR_OUTPUT_1
            STDOUT_OUTPUT_2
            STDERR_OUTPUT_2
          END_OF_OUTPUT
        )
      end
    end

    it "raises CommandNotExecutedError if the command hasn't been executed yet" do
      expect { subject.output }.to raise_error PeerCommander::Errors::CommandNotExecutedError
    end
  end

  describe "#duration" do
    let(:sleep_time) { 0.5 }

    it "returns the length of time the command took to run" do
      subject.execute
      expect(subject.duration).to be_within(0.1).of(sleep_time)
    end

    it "raises CommandNotExecutedError if the command hasn't been executed yet" do
      expect { subject.duration }.to raise_error PeerCommander::Errors::CommandNotExecutedError
    end

    context "without output to stderr" do
      let(:output_mode) { "stderr" }

      it "returns output which went to stderr" do
        subject.execute
        expect(subject.output).to eq "STDERR_OUTPUT\n"
      end
    end
  end

  describe "#executed" do
    it "returns true if a command has been executed" do
      subject.execute
      expect(subject.executed?).to be_truthy
    end

    context "when an exception is raised" do
      let(:command) { failing_command }

      it "returns true if an exception has been caught" do
        subject.execute
        expect(subject.executed?).to be_kind_of StandardError
      end
    end

    it "returns false before anything else has been executed" do
      expect(subject.executed?).to be_falsey
    end
  end

  describe "#exit_code" do
    context "when a command is run successfully" do
      it "is 0" do
        subject.execute
        expect(subject.exit_code).to eq 0
      end
    end

    context "when a command is run unsuccesfully" do
      let(:exit_code) { 1 }

      it "is 1" do
        subject.execute
        expect(subject.exit_code).to eq exit_code
      end
    end

    context "when a command had an exception" do
      let(:command) { failing_command }

      it "is nil" do
        subject.execute
        expect(subject.exit_code).to be_nil
      end
    end

    it "raises CommandNotExecutedError if the command hasn't been executed yet" do
      expect { subject.exit_code }.to raise_error PeerCommander::Errors::CommandNotExecutedError
    end
  end
end
