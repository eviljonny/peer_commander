RSpec.describe PeerCommander::CommandRunner do
  class FakeCommandOutput
    def initialize(success)
      @success = success
    end

    def success?
      @success
    end

    def failed?
      !success?
    end
  end

  subject { described_class.new(commands) }
  let(:commands) do
    [
      PeerCommander::Command.new("command1"),
      PeerCommander::Command.new("command2"),
      PeerCommander::Command.new("command3")
    ]
  end
  let(:executor) { double execute: command_output }
  let(:command_output) do
    [
      FakeCommandOutput.new(command_successes[0]),
      FakeCommandOutput.new(command_successes[1]),
      FakeCommandOutput.new(command_successes[2])
    ]
  end
  let(:command_successes) { [true, true, true] }

  before do
    allow(PeerCommander::ParallelExecutor).to receive(:new).and_return(executor)
  end

  describe "#execute" do
    it "Sends the commands and default parallelism of 1 to the ParallelExecutor" do
      expect(executor).to receive(:execute).with(commands, 1).and_return(command_output)
      subject.execute
    end

    it "accepts other values for parallelism" do
      expect(executor).to receive(:execute).with(commands, 3).and_return(command_output)
      subject.execute(parallelism: 3)
    end

    it "returns the command output" do
      allow(executor).to receive(:execute).with(commands, 1).and_return(command_output)
      expect(subject.execute).to be command_output
    end
  end

  describe "#success?" do
    context "when all commands succeeded" do
      it "returns true" do
        subject.execute
        expect(subject.success?).to be true
      end
    end

    context "when a command failed" do
      let(:command_successes) { [true, false, true] }

      it "returns false" do
        subject.execute
        expect(subject.success?).to be false
      end
    end

    context "when all commands failed" do
      let(:command_successes) { [false, false, false] }

      it "returns false" do
        subject.execute
        expect(subject.success?).to be false
      end
    end

    it "raises CommandNotExecutedError if the commands have not been executed yet" do
      expect { subject.success? }.to raise_error PeerCommander::Errors::CommandNotExecutedError
    end
  end

  describe "#failed?" do
    context "when all commands succeeded" do
      it "returns false" do
        subject.execute
        expect(subject.failed?).to be false
      end
    end

    context "when a command failed" do
      let(:command_successes) { [true, false, true] }

      it "returns true" do
        subject.execute
        expect(subject.failed?).to be true
      end
    end

    context "when all commands failed" do
      let(:command_successes) { [false, false, false] }

      it "returns true" do
        subject.execute
        expect(subject.failed?).to be true
      end
    end

    it "raises CommandNotExecutedError if the commands have not been executed yet" do
      expect { subject.failed? }.to raise_error PeerCommander::Errors::CommandNotExecutedError
    end
  end

  describe "#all_commands" do
    it "returns all of the command results" do
      subject.execute
      expect(subject.all_commands).to be command_output
    end

    it "raises CommandNotExecutedError if the commands have not been executed yet" do
      expect { subject.success? }.to raise_error PeerCommander::Errors::CommandNotExecutedError
    end
  end

  describe "#successful_commands" do
    let(:command_successes) { [true, false, true] }

    it "returns only commands which succeeded" do
      subject.execute
      expect(subject.successful_commands).to eq [command_output[0], command_output[2]]
    end

    it "raises CommandNotExecutedError if the commands have not been executed yet" do
      expect { subject.success? }.to raise_error PeerCommander::Errors::CommandNotExecutedError
    end
  end

  describe "#failed_commands" do
    let(:command_successes) { [false, true, false] }

    it "returns only commands which succeeded" do
      subject.execute
      expect(subject.failed_commands).to eq [command_output[0], command_output[2]]
    end

    it "raises CommandNotExecutedError if the commands have not been executed yet" do
      expect { subject.success? }.to raise_error PeerCommander::Errors::CommandNotExecutedError
    end
  end

  describe "#duration" do
    it "returns the duration" do
      subject.execute
      expect(subject.duration).to be_within(0.2).of(0)
    end
  end
end
