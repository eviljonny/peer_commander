RSpec.describe PeerCommander::ParallelExecutor do
  describe "#execute" do
    describe "return values" do
      context "with all successes" do
        let(:commands) do
          [
            PeerCommander::Command.new("spec/fixtures/test_cmd.sh 0 stdout"),
            PeerCommander::Command.new("spec/fixtures/test_cmd.sh 0 stderr")
          ]
        end

        it "returns all the results" do
          expect(subject.execute(commands, 2)).to eq(commands)
        end
      end

      context "with some failures" do
        let(:commands) do
          [
            PeerCommander::Command.new("spec/fixtures/test_cmd.sh 0 stdout"),
            PeerCommander::Command.new("spec/fixtures/not_exisiting_command")
          ]
        end

        it "returns all the results" do
          expect(subject.execute(commands, 2)).to eq(commands)
        end
      end

      context "with all failures" do
        let(:commands) do
          [
            PeerCommander::Command.new("spec/fixtures/not_exisiting_command"),
            PeerCommander::Command.new("spec/fixtures/other_not_existing_command")
          ]
        end

        it "returns all the results" do
          expect(subject.execute(commands, 2)).to eq(commands)
        end
      end

      context "with no commands" do
        it "returns an empty array" do
          expect(subject.execute([], 2)).to eq []
        end
      end
    end

    # I'm not really sure how to test this well, so most of the heavy lifiting
    # of the parallelism will be tested by inference in the feature tests,
    # so we're just going to excercise it a little here without doing much waiting
    # (to keep the test time down)
    describe "parallelism" do
      context "with fewer commands than parallelism" do
        let(:commands) { [PeerCommander::Command.new("echo command")] }

        it "completes without error" do
          result = subject.execute(commands, 2)
          expect(result.map(&:success?)).to eq([true])
          expect(result).to eq(commands)
        end
      end

      context "with exactly the number of commands as parallelism" do
        let(:commands) do
          [
            PeerCommander::Command.new("echo command1"),
            PeerCommander::Command.new("echo command2")
          ]
        end

        it "completes without error" do
          result = subject.execute(commands, 2)
          expect(result.map(&:success?)).to eq([true, true])
          expect(result).to eq(commands)
        end
      end

      context "with more commands than parallelism" do
        let(:commands) do
          [
            PeerCommander::Command.new("echo command1"),
            PeerCommander::Command.new("echo command2"),
            PeerCommander::Command.new("echo command3")
          ]
        end

        it "completes without error" do
          result = subject.execute(commands, 2)
          expect(result.map(&:success?)).to eq([true, true, true])
          expect(result).to eq(commands)
        end
      end

      it "raises an ArgumentError if passed parallelism less than 1" do
        expect { subject.execute(["echo test"], 0) }.to raise_error(ArgumentError, "Parallelism must be at least 1")
      end
    end
  end
end
