
lib = File.expand_path("lib", __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require_relative "lib/peer_commander/version"

Gem::Specification.new do |spec|
  spec.name                  = "peer_commander"
  spec.version               = PeerCommander::VERSION
  spec.authors               = ["Jonathan Harden"]
  spec.email                 = ["jonathan.harden@mydrivesolutions.com"]
  spec.required_ruby_version = ">= 2.4"

  spec.licenses              = ["MIT"]

  spec.summary               = "Run arbitrary system commands in parallel reporting on errors."
  spec.homepage              = "https://bitbucket.org/eviljonny/peer_commander/"

  spec.files                 = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end

  spec.require_paths = ["lib"]

  spec.add_dependency "concurrent-ruby", "~> 1"

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "pry-byebug", "~> 3"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
end
